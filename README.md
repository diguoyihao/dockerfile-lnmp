**第一步：安装Docker**
```

# 删除旧版本和相关依赖
yum remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine

# 安装依赖
yum install -y yum-utils \
    device-mapper-persistent-data \
    lvm2

# 国内Docker源，感觉中科大慢的可以使用阿里云
yum-config-manager \
    --add-repo \
    https://mirrors.ustc.edu.cn/docker-ce/linux/centos/docker-ce.repo

# 阿里云
yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 生成缓存
yum makecache fast

# 安装Docker
yum install docker-ce

# 开机启动启动Docker CE
systemctl enable docker

# 启动Docker CE
systemctl start docker

# 配置中国镜像
vim /etc/docker/daemon.json
{
    "registry-mirrors": ["http://docker.mirrors.ustc.edu.cn"]
}

# 测试Docker是否安装正确
docker run hello-world

# 添加内核参数
tee -a /etc/sysctl.conf <<-EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

# 重新加载 sysctl.conf
sysctl -p

```

**第二步：安装DockerCompose**

```
# 下载docker-compose文件
curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

# 赋予执行权限
chmod +x /usr/local/bin/docker-compose

# 查看版本
docker-compose --version

```

**第三步：执行DockerCompose自动化安装软件**

```
# 克隆源代码
git clone https://gitee.com/diguoyihao/dockerfile-lnmp.git /data

# 跳转到项目目录
cd /data/docker/bin

# 安装
docker-compose up -d --force-recreate --remove-orphans

```
